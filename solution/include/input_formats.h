#ifndef INPUT_FORMATS_H
#define INPUT_FORMATS_H

#include "img_worker.h"
#include <stdio.h>
#include <stdlib.h>

/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_PIXELS
  /* коды других ошибок  */
  };

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const* img );

struct errordesc {
  int64_t code;
  char* message;
};

 void read_message_handler(enum read_status status);

void write_message_hadler(enum write_status status);

struct bmp_header create_bmp(struct image* img, int64_t padding);

enum read_status read_pixels_in_img(FILE* in, struct image* img, uint64_t padding);


#endif
