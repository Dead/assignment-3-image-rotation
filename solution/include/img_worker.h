
#ifndef IMG_WORKER_H
#define IMG_WORKER_H

#include <stdint.h>

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct pixel { uint8_t b, g, r; };
 
struct image create_image(uint64_t width, uint64_t height);
struct image create_empty_image(void);
void delete_image(struct image*);


#endif
