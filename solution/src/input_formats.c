#include "bmp_format.h"
#include "img_worker.h"
#include <stdio.h>


#define BFTYPE 0x4D42
#define RESERVED 0
#define PLANES 1
#define COLORS_USED 0
#define COLORS_IMPORTANT 0
#define BIT_COUNT 24
#define OFFBITS sizeof(struct bmp_header)
#define COMPRESSION 0
#define BISIZE 40
#define X_PELS_PER_METER 2834
#define Y_PELS_PER_METER 2834
#define MESSAGE_OFFSET 5

enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER, 
  READ_INVALID_PIXELS
  };
  
/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR,
};

struct errordesc {
  int64_t code;
  char* message;
} errordesc[] = {
  {READ_OK, "Read was successful"},
  {READ_INVALID_SIGNATURE, "Invalid signature"},
  {READ_INVALID_BITS, "Invalid bits"},
  {READ_INVALID_HEADER, "Invalid header"},
  {READ_INVALID_PIXELS, "Invalid pixels"},
  {WRITE_OK, "Write was successful"},
  {WRITE_ERROR, "Write error"}
};

  void read_message_handler(enum read_status status){
  printf("%s", errordesc[status].message);
}

void write_message_hadler(enum write_status status){
  printf("%s", errordesc[status+MESSAGE_OFFSET].message);
}

 struct bmp_header create_bmp(struct image* img, uint64_t padding){
  return (struct bmp_header){
  .bfType = BFTYPE,
  .biWidth = img->width,
  .biHeight = img->height,
  .biSizeImage = (img->width * 3 + padding)  * img->height,
  .bfileSize = (img->width * 3 + padding)  * img->height + sizeof(struct bmp_header),
  .bfReserved = RESERVED,
  .bOffBits = OFFBITS,
  .biSize = BISIZE,
  .biPlanes = PLANES,
  .biBitCount = BIT_COUNT,
  .biCompression = COMPRESSION,
  .biXPelsPerMeter = X_PELS_PER_METER,
  .biYPelsPerMeter = Y_PELS_PER_METER,
  .biClrImportant = COLORS_IMPORTANT,
  .biClrUsed = COLORS_USED,
  };
  
}

enum read_status read_pixels_in_img(FILE* in, struct image* img, uint64_t padding){
    for (size_t i = 0; i < img->height; i++){
        if (fread(img->data + i * img->width, sizeof(struct pixel) * img->width, 1, in)!= 1){
          read_message_handler(READ_INVALID_PIXELS); 
          return READ_INVALID_PIXELS;
        }
        if (fseek(in, (long) padding, SEEK_CUR)){
          read_message_handler(READ_INVALID_PIXELS);
          return READ_INVALID_PIXELS;
        }
    }
    read_message_handler(READ_OK);
    return READ_OK;
}


enum read_status from_bmp( FILE* in, struct image* img ){
  struct bmp_header header;

  size_t count = fread(&header, sizeof(struct bmp_header), 1, in);

  if (header.bfType != BFTYPE){
    fclose(in);
    read_message_handler(READ_INVALID_SIGNATURE);
    return READ_INVALID_SIGNATURE;
  }
  if (count != 1){
    read_message_handler(READ_INVALID_HEADER);
    return READ_INVALID_HEADER;
  }
  if (header.biBitCount != 24){
     read_message_handler(READ_INVALID_BITS);
    return READ_INVALID_BITS;
  }

  *img = create_image(header.biWidth, header.biHeight);
  uint64_t padding = count_padding(img->width);

  enum read_status result = read_pixels_in_img(in, img, padding);
  return result;
}



enum write_status to_bmp( FILE* out, struct image* img ) {
    struct bmp_header bmp = create_bmp(img, count_padding(img->width));
    if (img == NULL || !out){
        return WRITE_ERROR;
    }
    // write header
    if (fwrite(&bmp, sizeof(struct bmp_header), 1, out)) {
    // write data
        for (size_t i = 0; i < img->height; i++) {
            fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);

            fseek(out, (long) count_padding(img->width), SEEK_CUR);
        }
        write_message_hadler(WRITE_OK);
        return WRITE_OK;
    }
    return WRITE_ERROR;
}



