#include <stdint.h>
#include <stdlib.h>

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct pixel { uint8_t b, g, r; };

struct image create_image(uint64_t width, uint64_t height){
    struct pixel *data = (struct pixel*)malloc(sizeof(struct pixel) * height * width);
    if (data) {
        return (struct image) {
                .width = width,
                .height = height,
                .data = data};
    }
    return (struct image){0};
}

struct image create_empty_image(void){
    return (struct image){0};
}


void delete_image(struct image* img){
    free(img->data);
    free(img);
}


